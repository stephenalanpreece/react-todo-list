import React, { useState, useRef,  useEffect } from 'react';
import ToDoList from './TodoList'
import { v4 as uuidv4 } from 'uuid';
import Button from 'react-bootstrap/Button';
import { Container, Row, Col} from 'react-bootstrap';

function App() {
const [todos, setTodos] = useState([])
const todoNameRef = useRef()
const LOCAL_STORAGE_KEY = 'todoApp.todos'


 
useEffect(() =>  {
  const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
  if (storedTodos) setTodos(storedTodos)
 }, [] )


useEffect (() => {
localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
}, [todos])

function toggleTodo (id) {
  const newTodos = [...todos]
  const todo = newTodos.find(todo => todo.id ===id)
  todo.complete = !todo.complete
  setTodos(newTodos)
}

function handleAddTodo(e) {
const name = todoNameRef.current.value
if (name === '') return 
setTodos(prevTodos => {
return [...prevTodos,  {id: uuidv4(), name: name, complete: false}]
})
todoNameRef.current.value = null
}

function handleClearTodos() {
  const newTodos = todos.filter(todo => !todo.complete)
  setTodos(newTodos)
}


  return (
    <>
    <Container fluid>
      <Row>
        <Col>
    <h1>YOUR TODO LIST</h1>
    </Col>
    </Row>
    <Row>
    <Col>
   <hr></hr>
    </Col>
    </Row>
    <Row>
    <Col>
   <div class="black-txt">This is your todo list, place an entry in the text box below and hit "Add Todo"! </div>
    </Col>
    </Row>
    <Row>
    <Col>
   <hr class="shorter-hr"></hr>
    </Col>
    </Row>
    
    <ToDoList todos={todos} toggleTodo={toggleTodo} />
    <Row>
      <Col>
    <div class="things-txt">{todos.filter(todo => !todo.complete).length} THING(S) TO DO</div>
    </Col>
    </Row>
    <input ref={todoNameRef} type="text" placeholder="Add a thing..." /><br></br>
    <Row>
      <Col>
    <Button variant="primary" size="lg"  onClick={handleAddTodo}>Add Todo</Button><br></br>
    </Col>
    </Row>
    <Row>
    <Col>
    <Button variant="primary" size="lg"  onClick={handleClearTodos}>Clear Completed</Button><br></br>
    </Col>
    </Row>
  
    </Container>
    </>
  )
}

export default App;
